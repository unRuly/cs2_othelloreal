#include "exampleplayer.h"
#include "math.h"

#define CORNER_VALUE 10000005
#define EDGE_VALUE 30
#define ADJACENT_VALUE -20
#define DIAGONAL_CORNER -4000
#define NEUTRAL 0

static int valueTable[8][8] = 
    {
    {CORNER_VALUE, ADJACENT_VALUE, EDGE_VALUE, EDGE_VALUE, EDGE_VALUE, EDGE_VALUE, ADJACENT_VALUE, CORNER_VALUE},
    {ADJACENT_VALUE, DIAGONAL_CORNER, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, DIAGONAL_CORNER, ADJACENT_VALUE},
    {EDGE_VALUE, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, EDGE_VALUE},
	{EDGE_VALUE, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, EDGE_VALUE},
    {EDGE_VALUE, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, EDGE_VALUE},
    {EDGE_VALUE, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, EDGE_VALUE},
    {ADJACENT_VALUE, DIAGONAL_CORNER, NEUTRAL, NEUTRAL, NEUTRAL, NEUTRAL, DIAGONAL_CORNER, ADJACENT_VALUE},
	{CORNER_VALUE, ADJACENT_VALUE, EDGE_VALUE, EDGE_VALUE, EDGE_VALUE, EDGE_VALUE, ADJACENT_VALUE, CORNER_VALUE}
    };


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side)
{
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
    // initialize my side and opponent's side
	if (side == WHITE)
    {
		mySide = WHITE;
		opponentsSide = BLACK;
	}
	else
	{
		mySide = BLACK;
		opponentsSide = WHITE;
	}
	
	// create internal board state

	myBoard = new Board();  

}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() 
{
	delete myBoard;
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
     /*Node wantedNode = minimax(myBoard, mySide, msLeft, 5);
     Move wantedMove = wantedNode.myMove;*/
     Move* wantedMove = constantScore(opponentsMove, msLeft);
     myBoard->doMove(wantedMove, mySide);
     return wantedMove;
			
	
}

Move *ExamplePlayer::constantScore(Move *opponentsMove, int msLeft)
{
	// update board given the opponent's last move
	myBoard->doMove(opponentsMove, opponentsSide);
	
	// make sure game is not over
	if (! myBoard->isDone())
	{
		// make sure we have moves
		if (myBoard->hasMoves(mySide))
		{
            int max_score = -100000;
			int current_score;
            Move wantedMove = Move(0,0);
			// search for moves by going through whole board and checking if moves legal
			for (int i = 0; i < 8; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					Move moveIdea = Move(i,j);
					current_score = score(myBoard, mySide);
					if (myBoard->checkMove(&moveIdea, mySide))
					{
						current_score += valueTable[i][j];
						if (current_score > max_score)
						{
							max_score = current_score;
							wantedMove=moveIdea;
						}
					}
                 
				}

			}
			Move *wanted = new Move(wantedMove.x, wantedMove.y);
			return wanted;
		}
		else
		{
			std::cerr << "no valid moves!" << std::endl;
			return NULL;
		}
	}
	else
	{
		std::cerr << "games over" << std::endl;
		return NULL;
	}
		
}

int ExamplePlayer::score(Board * inputBoard, Side side) {
    int score = 0;
    for (int i = 0; i < 8; i++)
    {
	    for (int j = 0; j < 8; j++)
	    {
			if (inputBoard->occupied(i,j))
			{
				if (inputBoard->get(side, i, j))
				{
					score += valueTable[i][j];
				}
			}
		}
    }
    return score;
}
/*
Node ExamplePlayer::minimax(Board *inputBoard, Side side, int msLeft, int recursions)
{
	std::vector<Node> myVector;
	Node wantedNode; 
	if (msLeft > 0)
	{
		// if we haven't yet done five recursions
		if (recursions < 5)
		{
			// make sure game is not over
			if (! inputBoard->isDone())
			{
				// if it's my turn and we have moves
				if (inputBoard->hasMoves(side))
					{
						// search for moves by going through whole board and checking if moves legal
						for (int i = 0; i < 8; i++)
						{
							for (int j = 0; j < 8; j++)
							{
								// create move idea for each one
								Move moveIdea = Move(i,j);
								// if it's a valid move
								if (inputBoard->checkMove(&moveIdea, side))
								{
									// copy input board and make move on it
									Board* newBoard = myBoard->copy();
									newBoard->doMove(&moveIdea, side);
									
									Side changeside = side == BLACK ? WHITE : BLACK;
									Node newNode = minimax(newBoard, changeside, msLeft, recursions + 1);
									newNode.myMove = moveIdea;
									myVector.push_back(newNode);
								}
							}
						}
					}
				}
			}
			if(side == mySide)
			{
				int max_score = -100000000;
				Move wantedMove = Move(0,0);
				for(std::vector<Node>::iterator it = myVector.begin(); it != myVector.end(); ++it)
				{
					if (it->myScore > max_score)
					{
						max_score = it->myScore;
						wantedMove = it->myMove;
						wantedNode = *it;
					}
				}
				return wantedNode;	
			}
			if(side != mySide)
			{
				int min_score = 100000000;
				Move wantedMove = Move(0,0);
				for(std::vector<Node>::iterator it = myVector.begin(); it != myVector.end(); ++it)
				{
					if (it->myScore < min_score)
					{
						min_score = it->myScore;
						wantedMove = it->myMove;
						wantedNode = *it;
					}
				}
				return wantedNode;	
			}
		// if # of recursions is 5
		else
		{
			wantedNode.myScore = score(inputBoard, mySide);
			return wantedNode;
		}
	}
} */
