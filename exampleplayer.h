#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
using namespace std;


struct Node{
	Move myMove;
	int myScore;
};
class ExamplePlayer {

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    Board *myBoard;
    Side opponentsSide, mySide;
	Move *constantScore(Move *opponentsMove, int msLeft);
	//Node minimax(Board *inputBoard, Side side, int msLeft, int recursions);
	int score(Board* myBoard, Side side);
	int current_score;
	
};

#endif
