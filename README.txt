1. We worked together to sort through the many ideas we had to 
implement strategy in our code. Through discussion, we tossed out a lot of
ideas that didn't seem feasible and tried to come up with some new ideas that
would incorporate suggestions from both sides. While coding, we worked at the same
time and debugged whatever we could and tried to talk through what we couldn't. 
We both agreed that we did an equal amount of work. 

2. Our first strategy we tried to implement was to count stones. It didn't take long
to realize that this strategy was severely flawed. Instead, we came up with a scoring
algorithm that incorporated a value table to put weights on edges and corners. 
We used this scoring algorithm in our minimax function (which we tried to implement
and got really close but still is just commented out.) We think this strategy will work 
because we placed sufficient weights on the corners and diagonal squares that the AI
will take the corners at any chance and try not to take the diagonal squares at all. 
And so we can beat ConstantTimePlayer!
